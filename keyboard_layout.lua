local wibox = require("wibox")
local awful = require("awful")
-- Keyboard map indicator and changer
kbdcfg = {}
kbdcfg.cmd = "setxkbmap"
kbdcfg.layout = { { "fr", "" , "Fr" }, { "ru", "phonetic_fr" , "Ru" } }
kbdcfg.current = 1
kbdcfg.widget = wibox.widget.textbox()
kbdcfg.widget:set_text(kbdcfg.layout[kbdcfg.current][3])
kbdcfg.switch = function ()
    kbdcfg.current = kbdcfg.current % #(kbdcfg.layout) + 1
    local t = kbdcfg.layout[kbdcfg.current]
    kbdcfg.widget:set_text(t[3])
    os.execute( kbdcfg.cmd .. " " .. t[1] .. " " .. t[2] )
end

-- Mouse bindings
kbdcfg.widget:buttons(
awful.util.table.join(awful.button({ }, 1, function () kbdcfg.switch() end))
)
