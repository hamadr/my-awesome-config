local wibox = require("wibox")

local refreshBattery = function()
    fh = assert(io.popen("acpi | cut -d, -f 2,3 -", "r"))
    batterywidget:set_text("⚡" .. fh:read("*l"))
    fh:close()
end

batterywidget = wibox.widget.textbox()
refreshBattery();
batterywidgettimer = timer({ timeout = 30 })
batterywidgettimer:connect_signal("timeout", refreshBattery)
batterywidgettimer:start()
